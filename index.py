import turtle #Modulo para la creacion de los objetos
import time #Modulo para controlar el tiempo de ejecucion
import msvcrt #Modulo para mantener ventana grafica abierta
import random

posponer = 0.1 #Constante para retrasar la ejecucion

#Marcador
score = 0
high_score = 0

#Configuraciond de ventana
wn = turtle.Screen() #funcion del llamdo para crear la ventana
wn.title("Juego de pon") #Titulo de la ventana
wn.bgcolor("black") #Fondo de la ventana
wn.setup(width = 600, height = 600) #Alto y ancho de la ventana
wn.tracer(0) #Animaciones Fluidas ???

#Cabeza de serpiente
cabeza = turtle.Turtle() # Creacion de objeto Turtle
cabeza.speed(0) #Velocidad del objeto
cabeza.shape("square") #forma del objeto
cabeza.color("white")#Color del objeto
cabeza.penup() #Sin dejar rastro del objeto
cabeza.goto(0,0) #Posicion inicial del objeto (0,0 - Centro de la ventana)
cabeza.direction = "stop" #Direccion inicial del objeto

#Comida de la serpiente

comida = turtle.Turtle()
comida.speed(0)
comida.shape("circle")
comida.color("red")
comida.penup()
comida.goto(0,100) #Posicion inicial del objeto (0,0 - Centro de la ventana)

#Marcador
texto = turtle.Turtle()
texto.speed(0)
texto.color("white")
texto.penup()
texto.hideturtle()
texto.goto(0,260)
texto.write("Score: 0 			High Score: 0", align="center", font=("Courier",15, "normal"))

#Cuerpo serpiente
segmentos = []

#Funciones

#Funciones de direccion
def arriba():
	cabeza.direction = "up"

def abajo():
	cabeza.direction = "down"

def derecha():
	cabeza.direction = "right"

def izquierda():
	cabeza.direction = "left"
#/*-------------------*/

def mov():
	if cabeza.direction == 'up': #Si la direccion del objeto es hacia arriba
		y = cabeza.ycor() #Obtener cordenada Y del objeto
		cabeza.sety(y + 20) #Mover en pixeles

	if cabeza.direction == 'down': #Si la direccion del objeto es hacia abajo
		y = cabeza.ycor() #Obtener cordenada Y del objeto
		cabeza.sety(y - 20) #Mover en pixeles

	if cabeza.direction == 'right': #Si la direccion del objeto es hacia derecha
		x = cabeza.xcor() #Obtener cordenada X del objeto
		cabeza.setx(x + 20) #Mover en pixeles

	if cabeza.direction == 'left': #Si la direccion del objeto es hacia izquierda
		x = cabeza.xcor() #Obtener cordenada X del objeto
		cabeza.setx(x - 20) #Mover en pixeles

#Teclado
wn.listen() #Para que la pantalla este atenta a las teclas que se presionan
wn.onkeypress(arriba, "Up")
wn.onkeypress(abajo, "Down")
wn.onkeypress(izquierda, "Left")
wn.onkeypress(derecha, "Right")

#Bucle principal
while True:
	wn.update() #Actualizacion de ventana

	#Colisiones Bordes
	if cabeza.xcor() > 280 or cabeza.xcor() < -280 or cabeza.ycor() > 280 or cabeza.ycor() < -280:
		time.sleep(1) #tiempo de espera despues de una colision
		cabeza.goto(0,0) #Devolver a poisicion inicial el objeto
		cabeza.direction = 'stop' #Parar movimiento del objeto
		
		#Reset marcador
		score = 0
		texto.clear()
		texto.write(f"Score: {score} 			High Score: {high_score}", align="center", font=("Courier",15, "normal"))

		#Bucle para ocultar los segmentos
		[i.hideturtle() for i in segmentos]
		segmentos.clear()


	#Detectar colision de cabeza con comida
	if cabeza.distance(comida) < 20:
		x = random.randint(-280, 280) #Crear numero random para nueva posicion de comida
		y = random.randint(-280, 280) #Crear numero random para nueva posicion de comida
		comida.goto(x,y)


		cuerpo = turtle.Turtle() # Creacion de objeto Turtle
		cuerpo.speed(0) #Velocidad del objeto
		cuerpo.shape("square") #forma del objeto
		cuerpo.color("grey")#Color del objeto
		cuerpo.penup() #Sin dejar rastro del objeto
		segmentos.append(cuerpo)

		#Aumentar marcador
		score += 10

		if score > high_score:
			high_score = score

		texto.clear()
		texto.write(f"Score: {score} 			High Score: {high_score}", align="center", font=("Courier",15, "normal"))

	#Mover el cuerpo
	totalSeg = len(segmentos)
	#Recorrer el rango de segmentos desde el mayor al menor sin contar el 0
	for index in range(totalSeg -1, 0, -1):
		#Obtener coordenadas
		x = segmentos[index - 1].xcor()
		y = segmentos[index - 1].ycor()
		#Asignamos las coordenadas al segmento
		segmentos[index].goto(x,y)

	#Validamos si el total de los segmentos es mayor a cero
	if totalSeg > 0:
		#Obtenemos Coordenadas
		x = cabeza.xcor()
		y = cabeza.ycor()
		#Asignamos coordenadas a los segmentos par que sigan al objeto 0
		segmentos[0].goto(x,y)

	mov()

	#Colisiones cuerpo
	for segmento in segmentos:
		if segmento.distance(cabeza) < 20:
			time.sleep(1)
			cabeza.goto(0,0)
			cabeza.direction = 'stop'

			#Bucle para ocultar los segmentos
			[i.hideturtle() for i in segmentos]
			segmentos.clear()

	time.sleep(posponer)

msvcrt.getch()